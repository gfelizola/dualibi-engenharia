<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dualibi Engenharia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="site-container" class="container">
            <?php include "header.php" ?>

            <section class="conteudo servicos row">
                <article class="col-xs-12 col-sm-8">
                    <p><b>Construção e Reforma de Edifícios</b><br>
                        Construção e reforma de edificações comerciais e residenciais.<br>
                        Cálculos estruturais para edificações<br>
                        Projetos Elétricos e Hidráulicos<br>
                        Revestimentos e acabamentos</p>
 
                    <p><b>Gerenciamento:</b><br>
                        Estudo de viabilidade econômica<br>
                        Orçamentos e especificações<br>
                        Cronogramas periódicos</p>

                    <p><b>Manutenção e Revitalização Predial</b><br>
                        Manutenção predial corretiva e preventiva- estrutural e em acabamentos:<br>
                        Pintura e revitalização de fechada<br>
                        Impermeabilização de áreas<br>
                        Manutenção e reforma em piscinas<br>
                        Projetos para captação e reaproveitamento de água</p>
 
                    <p><b>Gestão de residências de alto Padrão</b><br>
                        Manutenção preventiva<br>
                        Disponibilidade para emergências</p>


                    <p><img src="img/servicos/misc-pictures-2-1194500.jpg"></p>
                </article>
                <aside class="col-xs-12 col-sm-4">
                    <p><img src="img/servicos/roof-1171739.jpg"></p>
                    <p><img src="img/servicos/building-detail-1191991-1279x959.jpg"></p>
                    <p><img src="img/servicos/synagogue-1472753.jpg"></p>
                </aside>
            </section>

            <?php include "footer.php" ?>
        </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ec8407da-3915-41e8-8807-d83b605eec6a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
