<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dualibi Engenharia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="site-container" class="container">
            <?php include "header.php" ?>

            <section class="conteudo contato row">
                <article class="col-xs-12 col-sm-7">
                    <h2>Contato</h2>
                    <p>Para entrar em contato, preencha o formulário abaixo.</p>
                    <form id="formContato" action="envia.php" method="POST">
                        <div class="form-group">
                            <label for="nome">Nome completo*:</label>
                            <input type="text" name="nome" class="form-control required" id="nome" placeholder="meu nome">
                        </div>
                        <div class="form-group">
                            <label for="email">Email*:</label>
                            <input type="email" name="email" class="form-control required email" id="email" placeholder="email@joao.com">
                        </div>
                        <div class="form-group">
                            <label for="mensagem">Mensagem*:</label>
                            <textarea name="mensagem" id="mensagem" rows="3" class="form-control required"></textarea>
                        </div>
                          
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <button type="submit" class="btn btn-enviar btn-block btn-default">Enviar</button>
                                <div id="loading" class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                            <div id="feedback" class="col-xs-12 col-sm-8">
                                <div class="well hidden">
                                    <p class="text-danger text-center">Mensagem enviada</p>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <small>* campos obrigatórios</small>
                            </div>
                        </div>
                    </form>
                </article>
                <sidebar class="col-xs-12 col-sm-5">
                    <h3>Infos para contato</h3>
                    <p><a href="mailto:contato@duailibiengenharia.com.br" class="item">contato@duailibiengenharia.com.br</a></p>
                    <p>Telefones: <br>
                         - 55 11 3051 8302<br>
                         - 55 11 94739 7002</p>
                </sidebar>
            </section>

            <?php include "footer.php" ?>
        </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/vendor/jquery.validate.min.js"></script>
        <script src="js/vendor/messages_pt_BR.min.js"></script>
        <script src="js/vendor/jquery.form.min.js"></script>
        <script src="js/main.js"></script>

        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ec8407da-3915-41e8-8807-d83b605eec6a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
