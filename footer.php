<footer id="main-footer" class="row">
    <p>
        <a href="mailto:contato@dualibiengenharia.com.br" class="item">contato@dualibiengenharia.com.br</a>
        <span class="item telefone">
            <span>•</span>
            <span>55</span> 
            <span>11</span>
            <span>3051</span> 
            <span>8302</span>
        </span>

        <span class="item telefone">
            <span>•</span>
            <span>55</span>
            <span>11</span>
            <span>94739</span>
            <span>7002</span>
        </span>

        <span class="item direitos">
            <span>•</span> 
            <span>©</span>
            <span>copyright</span>
            <span>2015</span>
        </span>

        <span class="item">
            <span>•</span>
            <span>todos os direitos reservados</span>
        </span>
    </p>
</footer>