<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dualibi Engenharia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="site-container" class="container">
            <?php include "header.php" ?>

            <section class="conteudo empresa tela-texto row">
                <article class="col-xs-12 col-sm-offset-2 col-sm-8">
                    <p>A Duailibi Engenharia é uma empresa focada em reformas e manutenções prediais, que ao longo de 4 décadas adquiriu experiência em soluções integradas em diferentes mercados, gerenciando projetos diversos, como edifícios residenciais, construção e reformas comerciais e residenciais. Atuando em projetos para importantes organizações, como Cosipa, Grupo Pão de Açúcar, Rede Drogasil entre outras.</p>
                    <p>O objetivo principal da Duailibi Engenharia é oferecer a melhor solução aos seus clientes respeitando sempre os limites de prazo e budget.</p>
                    <p>Nossa equipe possui parcerias e profissionais com notória experiência nos mais diversos campos da engenharia civil atuando juntos há 30 anos, desde a fase de planejamento, coordenação, execução e entrega dos projetos.</p>
 
                    <p><b>Serviços:</b>

                        <ul>
                            <li>Construção e Reforma</li>
                            <li>Manutenção e Revitalização Predial</li>
                            <li>Gestão de Residências de alto Padrão</li>
                        </ul>
                    </p>

                </article>
            </section>

            <?php include "footer.php" ?>
        </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ec8407da-3915-41e8-8807-d83b605eec6a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
