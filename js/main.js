$(function() {
	if( $(".contato").length ){
		var loader = jQuery('#loading').hide();

		jQuery("#formContato").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				// Append error within linked label
				$( element )
					.closest( "form" )
						.find( "label[for='" + element.attr( "id" ) + "']" )
							.append( error );
			},
			submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					target: "#feedback",
					beforeSubmit: function(){
						loader.fadeIn(200);
					},
        			success: function(){
						loader.fadeOut(1000);
					},
					error: function(){
						loader.fadeOut(1000);
					}
				});
			}
		});

		// jQuery("#reset").click(function() {
		// 	v.resetForm();
		// });
	}

	if ( $('.home').length ) {
		var currentImg = 0;

		setInterval(function(){
			currentImg++;
			if ( currentImg >= $('.home img').length ) currentImg = 0;

			$('.home img').not(currentImg).fadeOut(1000);
			$('.home img').eq(currentImg).fadeIn(1000);
		},3000);

		$('.home img').hide();
		$('.home img').eq(0).fadeIn();
	};
});