<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dualibi Engenharia</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="site-container" class="container">
            <?php include "header.php" ?>

            <section class="conteudo empresa row">
                <article class="col-xs-12 col-sm-8">

                    <h3>Grupo Pão de Açúcar</h3>
                    <p>Manutenção em lojas: diagnóstico, contratação e acompanhamento dos serviços necessários, na área civil, em lojas do grupo.    
                        Residências: manutenções e acompanhamento da reforma e construção de diversas residências  de alto padrão.</p>
                                               
                    <h3>Reune</h3>
                    <p>Construção de três edifícios residenciais de alto padrão.</p>
                    <p>Construtora Newco: Responsável pelo Cliente Drogasil,e execução de reforma em sessenta lojas no estado de São Paulo e Minas Gerais.</p>
                     
                    <h3>SCG Engenharia</h3>
                    <p>Responsável pela fiscalização da construtora nas obras civis da implantação de nova unidade na Cosipa.</p>
                     
                    <h3>Projetos Recentes:</h3>
                    <p>Cão Resort - Hotel para cachorros<br>
                        Artemobi - Locação de móveis para eventos<br>
                        Lily Bijoux - Loja de acessórios femininos</p>

                </article>
                <aside class="col-xs-12 col-sm-4">
                    <p><img src="img/projetos/capacete.jpg"></p>
                    <p><img src="img/projetos/buildings-9-1478032-1279x852.jpg"></p>
                    <p><img src="img/projetos/building-1194504-1600x1200.jpg"></p>
                    <p><img src="img/projetos/excavator-1426495-1280x960.jpg"></p>
                </aside>
            </section>

            <?php include "footer.php" ?>
        </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "ec8407da-3915-41e8-8807-d83b605eec6a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
