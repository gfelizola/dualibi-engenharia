<?php
$arquivo = array_pop( explode("/", $_SERVER['SCRIPT_FILENAME']) );
?>
<header class="row">
    <div class="col-xs-12 col-sm-6">
        <h1 class="logo"><a href="index.php" class="text-hide">Dualibi Engenharia</a></h1>
    </div>

    <nav id="main-menu" class="clearfix col-xs-12">
        <ul>
            <?php
                $menus = [
                    "empresa.php"  => "Empresa",
                    "servicos.php" => "Serviços",
                    "projetos.php" => "Projetos",
                    "contato.php"  => "Contato",
                ];
            ?>
            <?php foreach ($menus as $link => $label): ?>
            <li class="col-xs-12 col-sm-3 <?php if($arquivo == $link){ echo "ativo"; }?>">
                <a href="<?php echo $link ?>"><?php echo $label ?></a>
            </li>
            <?php endforeach ?>
        </ul>
    </nav>
</header>